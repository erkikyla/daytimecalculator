var ctx = document.getElementById('myChart').getContext('2d'),
    firstDate,
    lastDate,
    dates, 
    values,
    dateLabels,
    datasetLabel,
    isDay,
    myChart = null;

function drawGraph(){
    isDay = document.getElementById("switch").checked;
    
    if(isDay){
        datasetLabel='Daytime length';
    }
    else{
        datasetLabel='Nighttime length'
    };

    try {
        checkCoordinates("latitude", 90);
        checkDateInput("fromDate");
        checkDateInput("toDate");
    }
    catch(err) {
        alert(err.message);
        return;
    }

    calculatePoints();

    if (myChart != null) {
        myChart.destroy();
    };

    myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dateLabels,
            datasets: [{
                label: datasetLabel,
                data: values,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        },
    });
}

function calculatePoints() {
    var currentDate;
    firstDate = new Date(document.getElementById("fromDate").value);
    lastDate = new Date(document.getElementById("toDate").value);
    dates = [];
    values = [];
    dateLabels = [];
    while (firstDate <= lastDate) {
        currentDate = new Date(firstDate);
        dates.push(currentDate);
        dateLabels.push(currentDate.toDateString());
        firstDate.setDate(firstDate.getDate() + 1);
    };
    dates.forEach(addValue);
}

function addValue(date) {
    if (isDay){
        values.push(daytimeLength(date).toFixed(2));
    }
    else {
        values.push((1440-daytimeLength(date)).toFixed(2));
    };
}