var screen = document.getElementById("screen");

function myFunction() {
    var date = new Date(document.getElementById("date").value)
    document.getElementById("screen").innerHTML = date.getUTCDate();
    return;
}

function main() {
    /*screen.innerHTML = "";
    screen.style.color = "black";*/
    try {
        checkInputs();
        daytime();
    }
    catch(err) {
        alert(err.message);
    }
}

function checkInputs() {
    checkCoordinates("latitude", 90);
    checkCoordinates("longitude", 180);
    checkDateInput("date");
}

function checkCoordinates(name, degree) {
    var input, numericalInput;
    input = document.getElementById(name).value;
    numericalInput = Number(input);
    if (input == "") {
        throw new Error("InputError: the " + name + " field is empty");
    }

    if (numericalInput > degree || numericalInput < -degree) {
        throw new RangeError("RangeError: The input of " + name + " is out of range [0," + degree + "]");
    }
}

function checkDateInput(name) {
    if (document.getElementById(name).value == "") {
        throw new Error("InputError: the " + name + " field is empty");
    };
}

function lat() {
    return toRadians(Number(document.getElementById("latitude").value))
}

function toRadians(degrees) {
    return Math.PI * degrees / 180
}

function toDegrees(radians) {
    return 180 * radians / Math.PI
}

function toHoursAndMinutes(minutes) {
    var hours;
    minutes = Math.round(minutes);
    hours = String(Math.floor(minutes / 60)).padStart(2,0);
    minutes = String(minutes % 60).padStart(2,0);
    return hours + ":" + minutes;
}

function toHours(minutes) {
    var hours = minutes/60;
    return hours.toFixed(2);
}

function gammaFunc(date) {
    var nrOfDays = 365,  
    day = date.getDate()+1,
    year = date.getFullYear(),
    daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    result;
    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
        nrOfDays = 366;
        daysInMonth[1] = 29;
    }

    for (var i = 0; i<date.getMonth(); i++) {
        day += daysInMonth[i];
    }
    result = 2 * Math.PI / nrOfDays * (day - 1)
    return result;
}

function declFunc(gamma) {
    var result = 0.006918 - 0.399912 * Math.cos(gamma) + 0.070257 *  Math.sin(gamma) - 
        0.006758 * Math.cos(2*gamma) + 0.000907 *  Math.sin(2*gamma) - 
        0.002697 * Math.cos(3*gamma) + 0.00148 *  Math.sin(3*gamma);
    return result;
}

function eqtimeFunc(gamma) {
    var result = 229.18 * (0.000075 + 0.001868 * Math.cos(gamma) - 0.032077 *  Math.sin(gamma) - 
        0.014615 * Math.cos(2*gamma) + 0.040829 *  Math.sin(2*gamma));
    return result;
}

function haFunc(gamma,latitude) {
    var decl = declFunc(gamma),
    expression = (Math.cos(toRadians(90.833)) - Math.sin(latitude) * Math.sin(decl)) / 
    (Math.cos(latitude) * Math.cos(decl)),
    result;
    if (expression <= -1) {
        result = Math.acos(-1);
    }
    else if (expression >= 1) {
        result = Math.acos(1);
    }
    else {
        result = Math.acos(expression);
    };
    return result;
}

function daytime() {
    var sunRise = sunRiseOrSet(1),
    sunSet = sunRiseOrSet(-1),
    lengthOfDay = sunSet-sunRise;
    sunRise = fixTimeOfDay(sunRise);
    sunSet = fixTimeOfDay(sunSet);
    screen.innerHTML = "Sunrise = " + toHoursAndMinutes(sunRise) + "<br>" +
        "Sunset = " + toHoursAndMinutes(sunSet) + "<br>" +
        "Length of night = " + (24-toHours(lengthOfDay)) + " h<br>" +
        "Length of day = " + toHours(lengthOfDay) + " h";
}

function fixTimeOfDay(minutes){
    if (minutes < 0) {
        minutes += 1440;
    };
    if (minutes >= 1440){
        minutes -= 1440
    };
    return minutes;
}

function sunRiseOrSet(sign, date = new Date(document.getElementById("date").value), 
            latitude = lat(), longitude = Number(document.getElementById("longitude").value),
            timeType = document.getElementById("timeType").value) {
    var gamma = gammaFunc(date),
    eqtime = eqtimeFunc(gamma),
    ha = toDegrees(haFunc(gamma,latitude)),
    time;
    if (timeType == "local") {
        longitude = 0;
    }
    time = 720 - 4 * (longitude + sign * ha) - eqtime;
    return time;
}

function daytimeLength(date, latitude = lat()) {
    var gamma = gammaFunc(date),
    ha = toDegrees(haFunc(gamma, latitude)),
    length = 8*ha;
    return length;
}

function isNight(latitude,longitude) {
    var date = new Date(document.getElementById("datetime").value),
    timeType = document.getElementById("timeType2").value,
    sunRise = sunRiseOrSet(1,date,Number(latitude),Number(longitude),timeType),
    sunSet = sunRiseOrSet(-1,date,Number(latitude),Number(longitude),timeType)
    time = date.getMinutes()+60*date.getHours();
    sunRise = fixTimeOfDay(sunRise);
    sunSet = fixTimeOfDay(sunSet);
    if(sunRise <= sunSet) {
        if (time >= sunRise && time <= sunSet) {
            return false;
        }
        else{
            return true;
        };
    }
    else {
        if (time < sunRise && time > sunSet) {
            return true;
        }
        else{
            return false;
        };
    }
}

function nightSwitch() {
    var nightSwitch = document.getElementById("nightSwitch");
    if (nightSwitch.checked == true){
        removeNight();
      } else {
        if(addNight()){
            nightSwitch.checked = true;
        };
      };
}