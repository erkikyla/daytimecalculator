
function onMapClick(e) {
    if (markerNotOnMap) {
        marker.addTo(mymap);
        markerNotOnMap = false;
    };
    document.getElementById("latitude").value = e.latlng.lat;
    document.getElementById("longitude").value = e.latlng.lng;
    marker.setLatLng(e.latlng);
}

function onMarkerDrag(e) {
    document.getElementById("latitude").value = e.latlng.lat;
    document.getElementById("longitude").value = e.latlng.lng;
}

function addNight() {
    darkLayerGroup.clearLayers();
    try{
        checkDateInput("datetime")
    }
    catch(err){
        alert(err.message);
        return true;
    }
    var n=3;
    for (let latitude = -90; latitude < 90; latitude=latitude+n) {
        for (let longitude = -180; longitude < 180; longitude=longitude+n) {
            if (isNight(latitude, longitude)) {
                darkLayerGroup.addLayer(L.rectangle([[latitude,longitude],[latitude+n,longitude+n]],
                    {color: 'black', opacity: 0.3, fillColor: 'black', fillOpacity: 0.3}));
            }
        }
    }
    return false;
}

function removeNight() {
    darkLayerGroup.clearLayers();
}

var mymap = L.map('mapid').setView([0,0], 2);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    minZoom: 1,
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZXJraWt5bGEiLCJhIjoiY2tvMzh6OWUxMHExbzJvczV4bmhqbmtmNSJ9.kaVuv3SlOn-8sCDL_AHEqQ'
}).addTo(mymap);

var marker = L.marker([0,0], {"draggable" : true}),
markerNotOnMap = true;

var darkLayerGroup = L.layerGroup().addTo(mymap);

//Events
mymap.on('click', onMapClick);
marker.on('move', onMarkerDrag);